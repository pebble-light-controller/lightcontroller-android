package com.example.lightcontroller

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.getpebble.android.kit.PebbleKit
import com.getpebble.android.kit.util.PebbleDictionary
import java.util.*

class MainActivity : AppCompatActivity() {

    val WATCHAPP_UUID: UUID = UUID.fromString("6c71cb35-b899-4112-ab04-244dc99eb58c")
    val WATCHAPP_FILENAME = "LightController.pbw"

    val TURN_OFF = 0
    val TURN_ON = 1

    private var mReceiver: PebbleKit.PebbleDataReceiver? = null
    var stateLbl: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        stateLbl = findViewById(R.id.stateLabel)
    }

    override fun onResume() {
        super.onResume()

        if (mReceiver == null) {
            mReceiver = object : PebbleKit.PebbleDataReceiver(WATCHAPP_UUID) {

                override fun receiveData(context: Context, id: Int, data: PebbleDictionary) {
                    PebbleKit.sendAckToPebble(applicationContext, id)
                    if (data.getInteger(TURN_ON) != null) {
                        stateLbl!!.text = "Turned on"
                    }
                    if (data.getInteger(TURN_OFF) != null) {
                        stateLbl!!.text = "Turned off"
                    }
                }
            }
            PebbleKit.registerReceivedDataHandler(applicationContext, mReceiver)
        }
    }
}
